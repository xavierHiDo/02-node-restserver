const User = require('./user.model');
const Role = require('./role.model');
const Category = require('./category.model');
const Product = require('./product.model');

module.exports = {
  User,
  Role,
  Category,
  Product
}
const { Router } = require('express');
const { check } = require('express-validator');
const { validate_jwt, validate_admin_role } = require('../middlewares');
const { validate_fields } = require('../middlewares/validate_fields');
const { post_product, get_products, get_product, put_product, delete_product } = require('../controllers/product.controller');
const { validate_product_id_exists, validate_category_id_exists } = require('../helpers/db_validators');
const router = Router();

router.get('/', get_products);

router.get('/:product_id', [
  check('product_id', 'no es un ID válido').isMongoId(),
  check('product_id').custom(validate_product_id_exists),
  validate_fields
], get_product);

router.post('/', [
  check('name', 'El nombre es obligatorio').not().isEmpty(),
  check('category', 'no es un ID válido').isMongoId(),
  check('category').custom(validate_category_id_exists),
  validate_fields,
  validate_jwt,
], post_product);

router.put('/:product_id', [
  validate_jwt,
  check('product_id', 'no es un ID válido').isMongoId(),
  check('product_id').custom(validate_product_id_exists),
  validate_fields
], put_product);

router.delete('/:product_id', [
  validate_jwt,
  validate_admin_role,
  check('product_id', 'no es un ID válido').isMongoId(),
  check('product_id').custom(validate_product_id_exists),
  validate_fields
], delete_product);

module.exports = router;
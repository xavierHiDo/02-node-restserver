const { response } = require('express');
const { ObjectId } = require('mongoose').Types;
const {User, Category, Product} = require('../models');

const allowed_collections = [
  'users',
  'categories',
  'products',
  'roles'
];

const searchUsers = async(word='', res=response) => {
  const is_mongo_id = ObjectId.isValid(word);

  if (is_mongo_id){
    const user = await User.findById(word);
    return res.json({
      results: (user) ? [user] : []
    });
  }

  const regx = new RegExp(word, 'i')
  const users = await User.find({
    $or: [{name: regx}, {email: regx}],
    $and: [{state: true}]
  });
  return res.json({
    results: users
  });
}

const searchCategories = async(word='', res=response) => {
  const is_mongo_id = ObjectId.isValid(word);

  if (is_mongo_id){
    const category = await Category.findById(word);
    return res.json({
      results: (category) ? [category] : []
    });
  }

  const regx = new RegExp(word, 'i')
  const categories = await Category.find({name: regx, state: true});
  return res.json({
    results: categories
  });
}

const searchProducts = async(word='', res=response) => {
  const is_mongo_id = ObjectId.isValid(word);

  if (is_mongo_id){
    const product = await Product.findById(word);
    return res.json({
      results: (product) ? [product] : []
    });
  }

  const regx = new RegExp(word, 'i')
  const products = await Product.find({name: regx, state: true});
  return res.json({
    results: products
  });
}

const search = async (req, res=response) => {
  const { collection, word } = req.params;

  if(!allowed_collections.includes(collection)){
    res.status(400).json({
      msg: `Las colecciones permitidas son: ${allowed_collections}`
    });
  }

  switch (collection) {
    case 'users':
      searchUsers(word, res);
      break;
    case 'categories':
      searchCategories(word, res);
      break;
    case 'products':
      searchProducts(word, res);
      break;
    default:
      res.status(500).json({
        msg: `Se le olvido hacer esta búsqueda`
      });
      break;
  }
};


module.exports = {
  search
}

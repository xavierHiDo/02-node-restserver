const { response } = require('express');
const bcryptjs = require('bcryptjs');
const User = require('../models/user.model');
const { generate_jwt } = require('../helpers/generate_jwt');
const { google_verify } = require('../helpers/google-verify');

const login = async (req, res= response) => {
  const { email, password } = req.body;

  try {
    const user = await User.findOne({email});

    if (!user) {
      return res.status(400).json({
        msj: 'Usuario / Password no son correctos -email'
      });
    }

    if (!user.state) {
      return res.status(400).json({
        msj: 'Usuario / Password no son correctos -state: false'
      });
    }

    const valid_password = bcryptjs.compareSync(password, user.password);
    if (!valid_password) {
      return res.status(400).json({
        msj: 'Usuario / Password no son correctos -password'
      });
    }

    const token  = await generate_jwt(user.id)

    res.json({
      msg: 'Login',
      token
    });

  } catch (error) {
    return res.status(500).json({
      msg: 'Hable con el administrador'
    });
  }

};

const google_login = async (req, res) => {
  const {id_token} = req.body;

  try {
    const {name, email, img} = await google_verify(id_token);
    let user = await User.findOne({email});
    if(!user){
      user = new User({name, email, password: ':P', img, google: true});
      await user.save();
    }

    if (!user.state) {
      return res.status(400).json({
        msj: 'Hable con el administrador, ususaio bloqueado'
      });
    }

    const token  = await generate_jwt(user.id)

    res.status(200).json({
      msg: 'Ok',
      token
    });
  } catch (error) {
    return res.status(400).json({
      msg: 'El token de Google no es valido'
    });
  }

};

module.exports = {
  login,
  google_login
}
const jwt = require('jsonwebtoken');
const User = require('../models/user.model');

const validate_jwt = async (req, res, next) => {
  const token  = req.header('Authorization');

  if (!token) {
    return res.status(401).json({
      msg: 'No hay token en la petición'
    });
  }

  try {
    const {uid} = jwt.verify(token, process.env.SECRETORPRIVATEKEY);
    const user = await User.findById(uid);

    if (!user){
      return res.status(401).json({
        msg: 'Token invalido -user deleted'
      })
    }

    if (!user.state){
      return res.status(401).json({
        msg: 'Token invalido -user state false'
      })
    }

    req.user = user;
    next();
  } catch (error) {
    console.log(error);
    return res.status(401).json({
      msg: 'Token invalido'
    });
  }
}

module.exports = {
  validate_jwt
}